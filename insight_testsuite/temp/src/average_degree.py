#!/usr/bin/env python
"""
	As tweets keep coming in from the input stream, we need an efficient 
	data structure to identify tweets that are older than 60 seconds compared
	to the current one, so we may delete them.An ideal choice might be a doubly
	linked list with insertion times that are of logN where N is the size of
	the cache. I don't know of any such in Pythonm so instead I have chosen
	a Min-Heap. We maintain tweets based on their creation time in a Min-Heap.
	When we want to delete older ones, we just keep popping rge heap. Being 
	a Min-Heap the oldest ones will appear first and we stop when find one 
	which is within our window. Insertion and deletion times are both of 
	order logN.

    In addition we have a dictionary that maintains the degree of each
	hashtag. Whenever we process a tweet consisting of N hashtags, we
	increment the degree of each hashtag by N - 1. We create an entry only
	when N - 1 is greater than one. In other wordss if we have a tweet with
	only one hashtag, we process it, but don't update the degree dictionary.
	If a tweet has no hashtags, we just ignore it. Likewise when we delete
	a tweet we decrement the degree of all its hashtags by N - 1, assuming
	the hashtag exists in the mix.

	We convert the creation time to an absolute time in seconds since Jan 1 of
	year 1, so we don't have any issues of what to do with tweets coming
	in around midnight on Dec 31 of any year.
"""
"""
	Given a time stamp such as 'Thu Nov 05 19:15:14 +0000 2015', convert
	it to seconds since Jan 1 of year 1.
"""
def time_to_sec(line):
	month_to_int = {'Jan':1,'Feb':2,'Mar':3,'Apr':4,'May':5,'Jun':6,'Jul':7,'Aug':8, 'Sep':9, 'Oct':10, 'Nov':11, 'Dec':12}

	created_at = re.match(r".*?created_at\":\"(.*?)\",\"id\".*", line)
	day, month, date, time, unkn, year = created_at.group(1).split()
	year = int(year)
	date = int(date)
	hh, mm, ss = map(int, time.split(':'))
	epoch_day = datetime.date.toordinal(datetime.date(year, month_to_int[month], date))
	creation_time = epoch_day * 24 * 3600 + hh * 3600 + mm * 60 + ss
	return creation_time

"""
	Given an input stream, extract hashtags from it.
"""
def extract_hashtags(line):
	hashtags = []
	x = re.match(r".*hashtags\":\[({\"text\".*}).*\],\"urls.*", line)
	if x is None:
		return hashtags
	y = x.group(1)
	for z in  re.findall(r"{\"text\":\".*?\",.*?}", y):
		h = re.match(r".*\"text\":\"(.*?)\",.*?}", z)
		hashtags.append(h.group(1))
	return hashtags

"""
	Add a twit to the twit-heap and increment degrees of its hashtags.
"""
def add_twit(degree, twit_heap, twit):
	heappush(twit_heap, twit)
	hashtags = twit[1]
	for i in hashtags:
		if len(hashtags) > 1:
			degree[i] += len(hashtags) - 1

"""
	Decrement the degree of hashtags of a twit. The caller is responsible
	for deleting the twit from heap.
"""
def delete_twit(degree, twit):
	hashtags = twit[1]
	for i in hashtags:
		if i in degree:
			degree[i] -= len(hashtags) - 1
			if degree[i] == 0:
				del degree[i]
"""
	Delete twits that are older than 60 seconds compared to the current
	twit, 'twit'
"""
def delete_older_twits(degree, twit_heap, twit):
	oldest_ts = int(twit[0]) - 60
	while twit_heap:
		tmp = heappop(twit_heap)
		if int(tmp[0]) >= oldest_ts:
			heappush(twit_heap, tmp)
			break
		else:
			delete_twit(degree, tmp)

"""
	Report the average of hashtags in the diegree dictionary. fout is the
	output stream.
"""
def report_degree(fout, degree):
	vsum = 0.0
	for k in degree.keys():
		vsum += degree[k]
	if len(degree) == 0:
		print >>fout, 0.00
	else:
		print >>fout, "%.2f" % (round(vsum / len(degree), 2))

"""
	Process an incoming twit and output the result to the output
	stream 'fout'
	A twit consists of a creation time in seconds and a list of hashtags.
"""
def process_twit(twit, fout):
	global prev_twit
	ts = twit[0]
	hashtags = twit[1]
	#
	# it's either the first tweet or one which is within 60 seconds compared
	# to a twit we processed last.
	#
	if prev_twit is None or ts <= prev_twit[0] + 60 or ts >= prev_twit[0] - 60:
		delete_older_twits(degree, twit_heap, twit)
		add_twit(degree, twit_heap, twit)
		prev_twit = twit
		report_degree(fout, degree)

if __name__ == "__main__":
	import sys
	import re
	import datetime
	from collections import defaultdict
	from heapq import heappush, heappop
	twit_heap = []
	prev_twit = None
	degree = defaultdict(int)

	if len(sys.argv) !=  3:
		print "usage: %s <input file> <output file>" % sys.argv[0]
		exit(1)

	fin = open(sys.argv[1])
	fout = open(sys.argv[2], 'w+')
	for line in fin.readlines():
		if not "hashtags" in line:
			continue
		line = line.strip()
		creation_time = time_to_sec(line)
		hashtags = extract_hashtags(line)
		if not hashtags:
			continue
		process_twit((creation_time, hashtags), fout)
